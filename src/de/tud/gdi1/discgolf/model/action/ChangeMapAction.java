package de.tud.gdi1.discgolf.model.action;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;


import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.Entity;
import eea.engine.event.basicevents.CollisionEvent;

/**
 * @author Ioannis
 */
public class ChangeMapAction implements Action{

		private Entity basket;
		
		public ChangeMapAction(Entity basket) {
			this.basket = basket;
		}
		

		@Override
		public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
			CollisionEvent collision = (CollisionEvent) event;
			Entity entity = collision.getCollidedEntity();
			
			if(entity == basket) {
				System.out.println("--------------------SCORE---------------------");
				// TODO
				sb.enterState(6);

			}
		}
	}
