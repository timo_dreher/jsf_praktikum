/**
 * 
 */
package de.tud.gdi1.discgolf.model.action;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.model.entities.Disc;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.Entity;
import eea.engine.event.basicevents.CollisionEvent;

/**
 * @author Timo Dreher
 *
 */
public class HitFloorAction implements Action {
	// Initialize necessary variables
	private Entity floor;
	private Entity disc;

	/**
	 * Constructor
	 */
	public HitFloorAction(Entity floor, Entity disc) {
		this.floor = floor;
		this.disc = disc;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		// Declare and get the collision
		CollisionEvent collision = (CollisionEvent) event;
		Entity entity = collision.getCollidedEntity();

		// Floor collides with disc
		if (entity == floor) {
			((Disc) disc).setDiscOnFloor(true);
		}
	}

}
