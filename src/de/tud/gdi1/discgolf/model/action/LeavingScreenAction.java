/**
 * 
 */
package de.tud.gdi1.discgolf.model.action;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import eea.engine.action.Action;
import eea.engine.component.Component;

/**
 * @author Timo Dreher
 *
 */
public class LeavingScreenAction implements Action {

	/**
	 * Constructor
	 */
	public LeavingScreenAction() {
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		// Disc leaves screen -> game over
		sb.enterState(5);
	}

}
