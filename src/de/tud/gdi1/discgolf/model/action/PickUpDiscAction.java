/**
 * 
 */
package de.tud.gdi1.discgolf.model.action;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.model.entities.Disc;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.Entity;
import eea.engine.event.basicevents.CollisionEvent;

/**
 * @author Timo Dreher
 *
 */
public class PickUpDiscAction implements Action {
	// Initialize necessary variables
	private Entity player;
	private Entity disc;

	/**
	 * Constructor
	 */
	public PickUpDiscAction(Entity player, Entity disc) {
		this.player = player;
		this.disc = disc;
	}

	@Override
	public void update(GameContainer container, StateBasedGame sb, int delta, Component event) {
		// Declare and get the collision
		CollisionEvent collision = (CollisionEvent) event;
		Entity entity = collision.getCollidedEntity();

		// Disc collides with player and is not mid air
		if (entity == player && disc.getPosition().y > player.getPosition().y + 20) {
			// Put disc back in players hand
			disc.setPosition(new Vector2f(player.getPosition().x + 34, player.getPosition().y - 19));
			((Disc) disc).setDiscInHand(true);
			((Disc) disc).setDiscOnFloor(false);
		}
	}

}
