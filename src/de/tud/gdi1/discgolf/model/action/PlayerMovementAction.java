/**
 * 
 */
package de.tud.gdi1.discgolf.model.action;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.model.entities.Disc;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.Entity;

/**
 * @author Timo Dreher
 *
 */
public class PlayerMovementAction implements Action {
	// Initialize necessary variables
	private Vector2f position;
	private Boolean rightDir;
	private Entity disc;

	/**
	 * Constructor
	 */
	public PlayerMovementAction(Vector2f position, Boolean rightDir, Entity disc) {
		this.position = position;
		this.rightDir = rightDir;
		this.disc = disc;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		// Disc is on floor
		if (((Disc) disc).getDiscOnFloor() == true) {
			// A is pressed and player within right screen boundary
			if (rightDir && position.x < 800) {
				// Move player to the right
				position.x += 5;
				// Player is within left boundary
			} else if (position.x > 0) {
				// Move player to the left
				position.x -= 5;
			}
		}
	}

}
