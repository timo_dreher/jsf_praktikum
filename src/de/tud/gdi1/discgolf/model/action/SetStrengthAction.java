/**
 * 
 */
package de.tud.gdi1.discgolf.model.action;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.model.entities.Throw;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.Entity;

/**
 * @author Timo Dreher
 *
 */
public class SetStrengthAction implements Action {
	// Initialize necessary variables
	private int input;
	private Entity dummyThrow;

	/**
	 * Constructor
	 */
	public SetStrengthAction(int input, Entity dummyThrow) {
		this.input = input;
		this.dummyThrow = dummyThrow;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		// Q is pressed and current strength is at least 7f
		if (input == 1 && ((Throw) this.dummyThrow).getStrength() >= 7f) {
			// Decrease current strength
			((Throw) this.dummyThrow).setStrength(((Throw) this.dummyThrow).getStrength() - 1f);
			// E is pressed and current strength is max 10f
		} else if (input == 2 && ((Throw) this.dummyThrow).getStrength() <= 10f) {
			// Increase current strength
			((Throw) this.dummyThrow).setStrength(((Throw) this.dummyThrow).getStrength() + 1f);
		}

	}

}
