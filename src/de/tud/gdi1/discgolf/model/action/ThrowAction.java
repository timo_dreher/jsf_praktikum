/**
 * 
 */
package de.tud.gdi1.discgolf.model.action;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.model.entities.Disc;
import de.tud.gdi1.discgolf.model.entities.Throw;
import de.tud.gdi1.discgolf.model.factory.ThrowFactory;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.basicevents.CollisionEvent;

/**
 * @author Timo Dreher
 *
 */
public class ThrowAction implements Action {
	// Initialize necessary variables
	private float strength;
	private Entity floor;
	private CollisionEvent collision;
	private Entity disc;
	private Entity discThrow = null;
	private Entity highscore;
	private float wind;

	/**
	 * Constructor
	 */
	public ThrowAction(Entity disc, Entity floor, CollisionEvent collision, Entity highscore) {
		this.disc = disc;
		this.floor = floor;
		this.collision = collision;
		this.highscore = highscore;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		// Declare and get the dummy throw
		Entity dummyThrow = StateBasedEntityManager.getInstance().getEntity(1, "dummyThrow");

		// Get wind and strength of dummy throw
		strength = ((Throw) dummyThrow).getStrength();
		wind = ((Throw) dummyThrow).getWind();

		// Disc is in players hand
		if (((Disc) disc).getDiscInHand()) {
			// Get the current mouse position
			final Vector2f targetPos = new Vector2f(gc.getInput().getMouseX(), gc.getInput().getMouseY());

			// Get the distance between current mouse position and disc and increase it
			// to make sure target position is off screen, only the angle is important
			final float dx = (targetPos.x - disc.getPosition().x) * 1000;
			final float dy = (targetPos.y - disc.getPosition().y) * 1000;

			// Calculate the new disc position by adding the differences
			final Vector2f newPos = new Vector2f(targetPos.x + dx, targetPos.y + dy);

			// Make a new disc throw and add it to the entity manager
			discThrow = new ThrowFactory(strength, disc.getPosition(), newPos, floor, collision, disc, highscore, wind)
					.createEntity();
			StateBasedEntityManager.getInstance().addEntity(sb.getCurrentStateID(), discThrow);
		}

	}

}
