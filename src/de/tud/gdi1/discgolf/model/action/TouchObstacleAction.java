package de.tud.gdi1.discgolf.model.action;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.view.Launch;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.Entity;
import eea.engine.event.basicevents.CollisionEvent;

public class TouchObstacleAction implements Action{
	
	private Entity obstacle;
	
	public TouchObstacleAction(Entity obstacle) {
		this.obstacle = obstacle;
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
		CollisionEvent collision = (CollisionEvent) event;
		Entity entity = collision.getCollidedEntity();	
		
		if (entity == obstacle) {
			System.out.println("!!!!!!!!!!!!!!! DANGER !!!!!!!!!!!!!!!!!");
			//TODO: Restart level
		}
	}

}
