/**
 * 
 */
package de.tud.gdi1.discgolf.model.entities;

import de.tud.gdi1.discgolf.model.interfaces.IDisc;
import eea.engine.entity.Entity;

/**
 * @author Timo Dreher
 *
 */
public class Disc extends Entity implements IDisc {
	// Initialize necessary variables
	private boolean DiscInHand;
	private boolean DiscOnFloor;

	/**
	 * Constructor
	 */
	public Disc(String id, boolean DiscInHand, boolean DiscOnFloor) {
		super(id);
		this.DiscInHand = DiscInHand;
		this.DiscOnFloor = DiscOnFloor;
	}

	@Override
	public boolean getDiscInHand() {
		return this.DiscInHand;
	}

	@Override
	public void setDiscInHand(boolean DiscInHand) {
		this.DiscInHand = DiscInHand;
	}

	@Override
	public boolean getDiscOnFloor() {
		return this.DiscOnFloor;
	}

	@Override
	public void setDiscOnFloor(boolean DiscOnFloor) {
		this.DiscOnFloor = DiscOnFloor;
	}

}
