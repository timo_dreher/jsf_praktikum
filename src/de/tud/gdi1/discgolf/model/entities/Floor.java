/**
 * 
 */
package de.tud.gdi1.discgolf.model.entities;

import eea.engine.entity.Entity;

/**
 * @author Timo Dreher
 *
 */
public class Floor extends Entity {

	/**
	 * Constructor
	 */
	public Floor(String id) {
		super(id);
	}

}
