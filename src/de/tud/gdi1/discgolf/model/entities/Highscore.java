/**
 * 
 */
package de.tud.gdi1.discgolf.model.entities;

import de.tud.gdi1.discgolf.model.interfaces.IHighscore;
import eea.engine.entity.Entity;

/**
 * @author Timo Dreher
 *
 */
public class Highscore extends Entity implements IHighscore {
	// Initialize necessary variables
	private int count;

	/**
	 * Constructor
	 */
	public Highscore(String id, int count) {
		super(id);
		this.count = count;
	}

	@Override
	public int getHighscore() {
		return this.count;
	}

	@Override
	public void setHighscore() {
		this.count++;
	}

}
