/**
 * 
 */
package de.tud.gdi1.discgolf.model.entities;

import de.tud.gdi1.discgolf.model.interfaces.IStrength;
import de.tud.gdi1.discgolf.model.interfaces.IWind;
import eea.engine.entity.Entity;

/**
 * @author Timo Dreher
 *
 */
public class Throw extends Entity implements IStrength, IWind {
	// Initialize necessary variables
	protected float strength;
	private float wind;

	/**
	 * Constructor
	 */
	public Throw(String id, float strength, float wind) {
		super(id);
		this.strength = strength;
		this.wind = wind;
	}

	@Override
	public float getStrength() {
		return this.strength;
	}

	@Override
	public void setStrength(float strength) {
		this.strength = strength;

	}

	@Override
	public float getWind() {
		return this.wind;
	}

	@Override
	public void setWind(float wind) {
		this.wind = wind;
	}

}
