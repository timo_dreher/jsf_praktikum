/**
 * 
 */
package de.tud.gdi1.discgolf.model.factory;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import eea.engine.component.render.ImageRenderComponent;
import eea.engine.entity.Entity;
import eea.engine.interfaces.IEntityFactory;

/**
 * @author Timo Dreher
 *
 */
public class BackgroundFactory implements IEntityFactory {
	// Initialize necessary variable
	private final String file;

	/**
	 * Constructor
	 */
	public BackgroundFactory(String file) {
		this.file = file;
	}

	@Override
	public Entity createEntity() {
		// Create a new background entity, set its position and add an image to it
		Entity background = new Entity("Game_Background");
		background.setPosition(new Vector2f(400, 300));
		try {
			background.addComponent(new ImageRenderComponent(new Image(file)));
		} catch (SlickException e) {
			System.err.println("Cannot find " + file);
			e.printStackTrace();
		}
		return background;
	}

}
