/**
 * 
 */
package de.tud.gdi1.discgolf.model.factory;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import eea.engine.component.render.ImageRenderComponent;
import eea.engine.entity.Entity;
import eea.engine.interfaces.IEntityFactory;

/**
 * @author Timo Dreher
 *
 */
public class BasketFactory implements IEntityFactory {
	// Initialize necessary variable
	private final String file;

	/**
	 * Constructor
	 */
	public BasketFactory(String file) {
		this.file = file;
	}

	@Override
	public Entity createEntity() {
		// Create a new basket entity, set its position, scale and add an image to it
		Entity basket = new Entity("Basket");
		basket.setPosition(new Vector2f(600, 390));
		basket.setScale(0.1f);
		try {
			basket.addComponent(new ImageRenderComponent(new Image(file)));
		} catch (SlickException e) {
			System.err.println("Cannot find " + file);
			e.printStackTrace();
		}
		return basket;
	}

}
