/**
 * 
 */
package de.tud.gdi1.discgolf.model.factory;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.discgolf.model.entities.Disc;
import eea.engine.component.render.ImageRenderComponent;
import eea.engine.entity.Entity;
import eea.engine.interfaces.IEntityFactory;

/**
 * @author Timo Dreher
 *
 */
public class DiscFactory implements IEntityFactory {
	// Initialize necessary variables
	private final String file;
	private final Vector2f playerPos;
	private Vector2f size;

	/**
	 * Constructor
	 */
	public DiscFactory(String file, Vector2f playerPos) {
		this.file = file;
		this.playerPos = playerPos;
	}

	@Override
	public Entity createEntity() {
		// Create disc entity, set its position, scale, size and add an image to it
		Entity disc = new Disc("Disc", true, false);
		disc.setPosition(new Vector2f(playerPos.x + 34, playerPos.y - 19));
		disc.setScale(0.03f);
		size = new Vector2f(20, 10);
		disc.setSize(size);
		try {
			disc.addComponent(new ImageRenderComponent(new Image(file)));
		} catch (SlickException e) {
			System.err.println("Cannot find " + file);
			e.printStackTrace();
		}
		return disc;
	}

}
