/**
 * 
 */
package de.tud.gdi1.discgolf.model.factory;

import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.discgolf.model.entities.Floor;
import eea.engine.entity.Entity;
import eea.engine.interfaces.IEntityFactory;

/**
 * @author Timo Dreher
 *
 */
public class FloorFactory implements IEntityFactory {
	// Initialize necessary variables
	private final Vector2f position;
	private final Vector2f size;
	String file;

	/**
	 * Constructor
	 */
	public FloorFactory(float x, float y, float width, float height) {
		this.position = new Vector2f(x, y);
		this.size = new Vector2f(width, height);
	}

	@Override
	public Entity createEntity() {
		// Create floor entity, set position and size
		Entity floor = new Floor("Floor" + Math.random());
		floor.setPosition(position);
		floor.setSize(size);

		return floor;
	}

}
