/**
 * 
 */
package de.tud.gdi1.discgolf.model.factory;

import org.newdawn.slick.geom.Vector2f;

import de.tud.gdi1.discgolf.model.entities.Highscore;
import eea.engine.entity.Entity;
import eea.engine.interfaces.IEntityFactory;

/**
 * @author Timo Dreher
 *
 */
public class HighscoreFactory implements IEntityFactory {

	/**
	 * Constructor
	 */
	public HighscoreFactory() {
	}

	@Override
	public Entity createEntity() {
		// Create highscore entity and set the position
		Entity highscore = new Highscore("Highscore", 0);
		highscore.setPosition(new Vector2f(670, 5));

		return highscore;
	}

}
