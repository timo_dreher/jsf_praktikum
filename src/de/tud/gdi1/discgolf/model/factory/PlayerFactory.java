/**
 * 
 */
package de.tud.gdi1.discgolf.model.factory;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import eea.engine.component.render.ImageRenderComponent;
import eea.engine.entity.Entity;
import eea.engine.interfaces.IEntityFactory;

/**
 * @author Timo Dreher
 *
 */
public class PlayerFactory implements IEntityFactory {
	// Initialize necessary variables
	private final String file;
	private Vector2f size;

	/**
	 * Constructor
	 */
	public PlayerFactory(String file) {
		this.file = file;
	}

	@Override
	public Entity createEntity() {
		// Create player entity, set position, scale, size and add an image to it
		Entity player = new Entity("Player");
		player.setPosition(new Vector2f(50, 385));
		player.setScale(0.4f);
		size = new Vector2f(30, 80);
		player.setSize(size);
		try {
			player.addComponent(new ImageRenderComponent(new Image(file)));
		} catch (SlickException e) {
			System.err.println("Cannot find " + file);
			e.printStackTrace();
		}
		return player;
	}

}
