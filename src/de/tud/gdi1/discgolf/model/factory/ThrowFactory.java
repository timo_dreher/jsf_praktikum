/**
 * 
 */
package de.tud.gdi1.discgolf.model.factory;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.model.entities.Disc;
import de.tud.gdi1.discgolf.model.entities.Highscore;
import de.tud.gdi1.discgolf.model.entities.Throw;
import eea.engine.action.Action;
import eea.engine.component.Component;
import eea.engine.entity.Entity;
import eea.engine.event.Event;
import eea.engine.event.basicevents.CollisionEvent;
import eea.engine.event.basicevents.LoopEvent;
import eea.engine.interfaces.IEntityFactory;

/**
 * @author Timo Dreher
 *
 */
public class ThrowFactory implements IEntityFactory {
	// Initialize necessary variables
	private float strength;
	private final Vector2f position;
	private Vector2f targetPos;
	private Entity floor;
	private CollisionEvent collision;
	private Entity disc;
	private Entity highscore;
	private float wind;

	/**
	 * Constructor
	 */
	public ThrowFactory(float strength, Vector2f position, Vector2f targetPosition, Entity floor,
			CollisionEvent collision, Entity disc, Entity highscore, float wind) {
		this.strength = strength;
		this.position = position;
		this.targetPos = targetPosition;
		this.floor = floor;
		this.collision = collision;
		this.disc = disc;
		this.highscore = highscore;
		this.wind = wind;
	}

	@Override
	public Entity createEntity() {
		// Create throw entity
		Entity discThrow = new Throw("Throw", strength, wind);

		// Declaring necessary variables

		// The acceleration of gravity (m/s^2)
		final double g = -9.81;
		// The mass of a standard frisbee in kilograms
		final double m = 0.175;
		// The density of air in kg/m^3
		final double RHO = 1.23;
		// The area of a standard frisbee
		final double AREA = 0.0568;
		// The lift coefficient at alpha = 0
		final double CL0 = 0.1;
		// The lift coefficient dependent on alpha
		final double CLA = 1.4;
		// The drag coefficient at alpha = 0
		final double CD0 = 0.08;
		// The drag coefficient dependent on alpha
		final double CDA = 2.72;
		// Alpha
		final double ALPHA0 = -4;

		// Calculation of the lift coefficient
		double cl = CL0 + CLA * Math.PI / 180;
		// Calculation of the drag coefficient
		double cd = CD0 + CDA * Math.pow(ALPHA0 * Math.PI / 180, 2);

		// Creating new loop event for the throw
		Event mainEvent = new LoopEvent();
		mainEvent.addAction(new Action() {

			@Override
			public void update(GameContainer gc, StateBasedGame sb, int delta, Component event) {
				// Declare and get the collision
				Entity entity = collision.getCollidedEntity();

				// Calculating the target and current position differences and rotation
				float dx = (targetPos.x - disc.getPosition().x);
				float dy = (targetPos.y - disc.getPosition().y);
				float rotation = (float) Math.toDegrees(Math.atan2(dy, dx));

				// Declaring the velocities
				double veloX = ((Throw) discThrow).getStrength();
				double veloY = ((Throw) discThrow).getStrength();

				// Continuously decreasing throw strength
				((Throw) discThrow).setStrength((float) (((Throw) discThrow).getStrength() * 0.99));

				// The change in velocity in the y direction obtained setting the
				// net force equal to the sum of the gravitational force and the
				// lift force and solving for delta v
				double deltavy = (RHO * Math.pow(veloX, 2) * AREA * cl / 2 / m + g);
				// The change in velocity in the x direction, obtained by
				// solving the force equation for delta v. (The only force
				// present is the drag force).
				double deltavx = -RHO * Math.pow(veloX, 2) * AREA * cd;

				// Calculating the new velocities
				veloX = veloX + deltavx;
				veloY = veloY + deltavy;

				// Disc is currently not colliding with the floor and target position is on the
				// right of the disc
				if (entity != floor && targetPos.x > disc.getPosition().x) {
					// Calculating the new disc positions
					position.x += Math.cos(Math.toRadians(rotation))
							* (((Throw) discThrow).getStrength() + ((Throw) discThrow).getWind());
					position.y += Math.sin(Math.toRadians(rotation))
							* (((Throw) discThrow).getStrength() + ((Throw) discThrow).getWind()) - veloY;
					// Disc is currently not colliding with the floor and target position is on the
					// left of the disc
				} else if (entity != floor && targetPos.x < disc.getPosition().x) {
					// Calculating the new disc positions
					position.x += Math.cos(Math.toRadians(rotation))
							* (((Throw) discThrow).getStrength() - ((Throw) discThrow).getWind());
					position.y += Math.sin(Math.toRadians(rotation))
							* (((Throw) discThrow).getStrength() - ((Throw) discThrow).getWind()) - veloY;
				}
			}

		});

		// Add the created disc position calculation to the throw
		discThrow.addComponent(mainEvent);

		// Increase the highscore and set flag
		((Highscore) highscore).setHighscore();
		((Disc) disc).setDiscInHand(false);

		return discThrow;
	}

}
