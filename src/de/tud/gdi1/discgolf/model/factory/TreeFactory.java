package de.tud.gdi1.discgolf.model.factory;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import eea.engine.component.render.ImageRenderComponent;
import eea.engine.entity.Entity;
import eea.engine.interfaces.IEntityFactory;

/**
 * 
 * @author Ioannis
 *
 */
public class TreeFactory implements IEntityFactory {

	
	private final String file;
	private int posX;
	private int posY;
	
	public TreeFactory(String file,int posX, int posY) {
		this.file = file;
		this.posX = posX;
		this.posY = posY;
	}
	@Override
	public Entity createEntity() {
		Entity tree = new Entity("Tree");
		tree.setPosition(new Vector2f(posX , posY));
		tree.setScale(0.5f);
		try {
			tree.addComponent(new ImageRenderComponent(new Image(file)));
		} catch (SlickException e) {
			System.err.println("Cannot find " + file);
			e.printStackTrace();
		}
		return tree;

	}

}
