/**
 * 
 */
package de.tud.gdi1.discgolf.model.interfaces;

/**
 * @author Timo Dreher
 *
 */
public interface IDisc {

	// Indicates if the disc is in the players hand
	public boolean getDiscInHand();

	// Sets a flag, indicating if the disc is in the players hand
	public void setDiscInHand(boolean DiscInHand);

	// Indicates if the disc is on the floor
	public boolean getDiscOnFloor();

	// Sets a flag, indicating if the disc in on the floor
	public void setDiscOnFloor(boolean DiscOnFloor);
}
