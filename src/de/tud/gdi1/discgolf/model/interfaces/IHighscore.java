/**
 * 
 */
package de.tud.gdi1.discgolf.model.interfaces;

/**
 * @author Timo Dreher
 *
 */
public interface IHighscore {
	// Returns the current highscore
	public int getHighscore();

	// Sets the current highscore
	public void setHighscore();
}
