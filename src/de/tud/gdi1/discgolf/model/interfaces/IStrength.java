/**
 * 
 */
package de.tud.gdi1.discgolf.model.interfaces;

/**
 * @author Timo Dreher
 *
 */
public interface IStrength {
	// Returns the current strength
	public float getStrength();

	// Sets the current strength
	public void setStrength(float strength);
}
