/**
 * 
 */
package de.tud.gdi1.discgolf.model.interfaces;

/**
 * @author Timo Dreher
 *
 */
public interface IWind {
	// Returns the current wind
	public float getWind();

	// Sets the current wind
	public void setWind(float wind);
}
