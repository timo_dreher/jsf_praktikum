/**
 * 
 */
package de.tud.gdi1.discgolf.view;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.model.factory.BackgroundFactory;
import eea.engine.action.basicactions.ChangeStateAction;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.basicevents.KeyPressedEvent;

/**
 * @author Timo Dreher
 *
 */
public class AboutGameState extends BasicGameState {
	// Initialize the state identifier and entity manager
	private int stateID;
	private StateBasedEntityManager entityManager;

	/**
	 * Constructor
	 */
	public AboutGameState(int sid) {
		stateID = sid;
		entityManager = StateBasedEntityManager.getInstance();
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sb) throws SlickException {
		// Set background in game
		entityManager.addEntity(stateID, new BackgroundFactory("/assets/about.png").createEntity());

		// Press ESC to go back to main menu
		Entity escListener = new Entity("ESC_Listener");
		KeyPressedEvent escPressed = new KeyPressedEvent(Input.KEY_ESCAPE);
		escPressed.addAction(new ChangeStateAction(Launch.MAINMENU_STATE));
		escListener.addComponent(escPressed);
		entityManager.addEntity(stateID, escListener);
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		// StateBasedEntityManager renders all entities
		entityManager.renderEntities(container, game, g);

		// Controls text
		g.setColor(Color.black);
		g.drawString("Left Mouse: Throw Disc", 470, 200);
		g.drawString("A: Move Left", 470, 220);
		g.drawString("D: Move Right", 470, 240);
		g.drawString("Q: Decrease Throw Strength", 470, 260);
		g.drawString("E: Increase Throw Strength", 470, 280);
		g.drawString("ESC: Back to Main Menu", 470, 300);

		// Controls title
		Font controls = new Font("Monospaced", Font.BOLD, 28);
		TrueTypeFont ttf1 = new TrueTypeFont(controls, true);
		ttf1.drawString(500, 150, "Controls", Color.black);

		// About text
		g.drawString("Disc Golf is a Frisbeesport where", 70, 200);
		g.drawString("the goal is to hit each basket of", 70, 220);
		g.drawString("the current curse with as few throws", 70, 240);
		g.drawString("as possible. The highscore is counted", 70, 260);
		g.drawString("overall courses.", 70, 280);
		g.drawString("The game simplifies the rules in", 70, 300);
		g.drawString("order to keep it easy and fun.", 70, 320);

		// About title
		Font about = new Font("Monospaced", Font.BOLD, 28);
		TrueTypeFont ttf2 = new TrueTypeFont(about, true);
		ttf2.drawString(170, 150, "About", Color.black);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		// StateBasedEntityManager updates all entities
		entityManager.updateEntities(container, game, delta);
	}

	@Override
	public int getID() {
		return stateID;
	}

}
