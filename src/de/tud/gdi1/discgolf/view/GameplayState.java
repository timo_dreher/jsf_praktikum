/**
 * 
 */
package de.tud.gdi1.discgolf.view;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.model.action.ChangeMapAction;
import de.tud.gdi1.discgolf.model.action.HitFloorAction;
import de.tud.gdi1.discgolf.model.action.LeavingScreenAction;
import de.tud.gdi1.discgolf.model.action.PickUpDiscAction;
import de.tud.gdi1.discgolf.model.action.PlayerMovementAction;
import de.tud.gdi1.discgolf.model.action.SetStrengthAction;
import de.tud.gdi1.discgolf.model.action.ThrowAction;
import de.tud.gdi1.discgolf.model.entities.Highscore;
import de.tud.gdi1.discgolf.model.entities.Throw;
import de.tud.gdi1.discgolf.model.factory.BackgroundFactory;
import de.tud.gdi1.discgolf.model.factory.BasketFactory;
import de.tud.gdi1.discgolf.model.factory.DiscFactory;
import de.tud.gdi1.discgolf.model.factory.FloorFactory;
import de.tud.gdi1.discgolf.model.factory.HighscoreFactory;
import de.tud.gdi1.discgolf.model.factory.PlayerFactory;
import eea.engine.action.basicactions.ChangeStateAction;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.Event;
import eea.engine.event.basicevents.CollisionEvent;
import eea.engine.event.basicevents.KeyDownEvent;
import eea.engine.event.basicevents.KeyPressedEvent;
import eea.engine.event.basicevents.LeavingScreenEvent;
import eea.engine.event.basicevents.MouseClickedEvent;

/**
 * @author Timo Dreher
 *
 */
public class GameplayState extends BasicGameState {
	// Initialize necessary variables
	private int stateID;
	private StateBasedEntityManager entityManager;
	private Entity player;
	private Entity disc;
	private Entity basket;
	private Entity floor;
	private Entity highscore;
	private int random;
	private float wind;

	/**
	 * Constructor
	 */
	public GameplayState(int sid) {
		stateID = sid;
		entityManager = StateBasedEntityManager.getInstance();
	}

	/**
	 * One time before start
	 */
	@Override
	public void init(GameContainer gc, StateBasedGame sb) throws SlickException {
		// Set background in game
		entityManager.addEntity(stateID, new BackgroundFactory("/assets/background.png").createEntity());

		// Press ESC to go back to main menu
		Entity escListener = new Entity("ESC_Listener");
		KeyPressedEvent escPressed = new KeyPressedEvent(Input.KEY_ESCAPE);
		escPressed.addAction(new ChangeStateAction(Launch.MAINMENU_STATE));
		escListener.addComponent(escPressed);
		entityManager.addEntity(stateID, escListener);

		// Get a random number to set the wind strength randomly
		random = (int) (Math.random() * 7);

		// Wind strength ranges from -1.5f to 1.5f
		if (random == 0) {
			wind = -1.5f;
		} else if (random == 1) {
			wind = -1f;
		} else if (random == 2) {
			wind = -0.5f;
		} else if (random == 3) {
			wind = 0f;
		} else if (random == 4) {
			wind = 0.5f;
		} else if (random == 5) {
			wind = 1f;
		} else {
			wind = 1.5f;
		}

		// Declare new throw and add it to the entity manager
		Entity dummyThrow = new Throw("dummyThrow", 8f, wind);
		entityManager.addEntity(stateID, dummyThrow);

		// Press Q or E to set the strength
		Entity strengthListener = new Entity("Strength Listener");
		KeyPressedEvent onePressed = new KeyPressedEvent(Input.KEY_Q);
		KeyPressedEvent twoPressed = new KeyPressedEvent(Input.KEY_E);
		onePressed.addAction(new SetStrengthAction(1, dummyThrow));
		twoPressed.addAction(new SetStrengthAction(2, dummyThrow));
		strengthListener.addComponent(onePressed);
		strengthListener.addComponent(twoPressed);
		entityManager.addEntity(stateID, strengthListener);

		// Initialize player
		player = new PlayerFactory("/assets/player.png").createEntity();
		entityManager.addEntity(stateID, player);

		// Initialize basket
		basket = new BasketFactory("/assets/basket.png").createEntity();
		entityManager.addEntity(stateID, basket);

		// Initialize floor
		floor = new FloorFactory(0, 425, 8000, 5).createEntity();
		entityManager.addEntity(stateID, floor);

		// Initialize disc
		disc = new DiscFactory("/assets/disc.png", player.getPosition()).createEntity();

		// Initialize highscore
		highscore = new HighscoreFactory().createEntity();
		entityManager.addEntity(stateID, highscore);

		// Collision with floor
		CollisionEvent collision = new CollisionEvent();
		collision.addAction(new HitFloorAction(floor, disc));
		disc.addComponent(collision);

		// Collision with basket
		CollisionEvent scoreBasket = new CollisionEvent();
		scoreBasket.addAction(new ChangeMapAction(basket));
		disc.addComponent(scoreBasket);

		// Collision with player
		CollisionEvent pickDisc = new CollisionEvent();
		pickDisc.addAction(new PickUpDiscAction(player, disc));
		disc.addComponent(pickDisc);

		entityManager.addEntity(stateID, disc);

		// Click mouse to throw disc at mouse position
		Entity mouseClickedListener = new Entity("MouseClickedListener");
		MouseClickedEvent mouseClicked = new MouseClickedEvent();
		mouseClicked.addAction(new ThrowAction(disc, floor, collision, highscore));
		mouseClickedListener.addComponent(mouseClicked);

		// Disc leaving screen results in losing game
		Event mainEvent = new LeavingScreenEvent();
		mainEvent.addAction(new LeavingScreenAction());
		disc.addComponent(mainEvent);

		entityManager.addEntity(stateID, mouseClickedListener);

		// Press A or D to move left or right
		Entity keyPressedListener = new Entity("KeyPressedListener");
		KeyDownEvent keyPressedA = new KeyDownEvent(Input.KEY_A);
		KeyDownEvent keyPressedD = new KeyDownEvent(Input.KEY_D);
		keyPressedA.addAction(new PlayerMovementAction(player.getPosition(), false, disc));
		keyPressedD.addAction(new PlayerMovementAction(player.getPosition(), true, disc));
		keyPressedListener.addComponent(keyPressedA);
		keyPressedListener.addComponent(keyPressedD);

		entityManager.addEntity(stateID, keyPressedListener);
	}

	/**
	 * On every frame
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics g) throws SlickException {
		// StateBasedEntityManager renders all entities
		entityManager.renderEntities(gc, sb, g);

		// Get global highscore and dummy throw
		Highscore globalHighscore = ((Highscore) entityManager.getEntity(this.stateID, "Highscore"));
		Throw dummyThrow = ((Throw) entityManager.getEntity(this.stateID, "dummyThrow"));

		// Draw highscore on screen
		g.setColor(Color.white);
		g.drawString("Highscore: " + ((Highscore) globalHighscore).getHighscore(), 670, 5);

		// Declare various strength visualization assets
		Image max = new Image("/assets/full.png");
		Image veryStrong = new Image("/assets/very_Strong.png");
		Image strong = new Image("/assets/strong.png");
		Image moderate = new Image("/assets/moderate.png");
		Image weak = new Image("/assets/weak.png");
		Image min = new Image("/assets/very_Weak.png");

		// Draw strength on screen
		g.drawString("Strength", 35, 5);

		// Draw corresponding image to the current strength
		if (((Throw) dummyThrow).getStrength() == 6) {
			g.drawImage(min, 20, 15);
		} else if (((Throw) dummyThrow).getStrength() == 7) {
			g.drawImage(weak, 20, 15);
		} else if (((Throw) dummyThrow).getStrength() == 8) {
			g.drawImage(moderate, 20, 15);
		} else if (((Throw) dummyThrow).getStrength() == 9) {
			g.drawImage(strong, 20, 15);
		} else if (((Throw) dummyThrow).getStrength() == 10) {
			g.drawImage(veryStrong, 20, 15);
		} else {
			g.drawImage(max, 20, 15);
		}

		// Declare various wind visualization assets
		Image wind = new Image("/assets/wind.png");
		Image windL1 = new Image("/assets/windl1.png");
		Image windL2 = new Image("/assets/windl2.png");
		Image windL3 = new Image("/assets/windl3.png");
		Image windR1 = new Image("/assets/windr1.png");
		Image windR2 = new Image("/assets/windr2.png");
		Image windR3 = new Image("/assets/windr3.png");

		// Draw wind on screen
		g.drawString("Wind", 375, 5);

		// Draw corresponding image to the current wind
		if (dummyThrow.getWind() == -1.5f) {
			g.drawImage(windL3, 350, 20);
		} else if (dummyThrow.getWind() == -1f) {
			g.drawImage(windL2, 350, 20);
		} else if (dummyThrow.getWind() == -0.5f) {
			g.drawImage(windL1, 350, 20);
		} else if (dummyThrow.getWind() == 0f) {
			g.drawImage(wind, 350, 20);
		} else if (dummyThrow.getWind() == 0.5f) {
			g.drawImage(windR1, 350, 20);
		} else if (dummyThrow.getWind() == 1f) {
			g.drawImage(windR2, 350, 20);
		} else {
			g.drawImage(windR3, 350, 20);
		}
	}

	/**
	 * Before every frame
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta) throws SlickException {
		// StateBasedEntityManager updates all entities
		entityManager.updateEntities(gc, sb, delta);
	}

	@Override
	public int getID() {
		return stateID;
	}

}
