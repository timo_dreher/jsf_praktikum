/**
 * 
 */
package de.tud.gdi1.discgolf.view;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import eea.engine.entity.StateBasedEntityManager;

/**
 * @author Timo Dreher
 * 
 */
public class Launch extends StateBasedGame {
	// Initialize states, represented as integers
	public static final int MAINMENU_STATE = 0;
	public static final int GAMEPLAY_STATE = 1;
	public static final int LEVEL2 = 2;
	public static final int LEVEL3 = 3;
	public static final int ABOUTGAME_STATE = 4;
	public static final int GAMEOVER_STATE = 5;
	public static final int WONGAME_STATE = 6;

	/**
	 * Constructor
	 */
	public Launch() {
		// Name of the game
		super("Disc Golf");
	}

	public static void main(String[] args) throws SlickException {
		// Set the library path, depends on OS
		// Windows
		if (System.getProperty("os.name").toLowerCase().contains("windows")) {
			System.setProperty("org.lwjgl.librarypath", System.getProperty("user.dir") + "/native/windows");
			// Mac
		} else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
			System.setProperty("org.lwjgl.librarypath", System.getProperty("user.dir") + "/native/macosx");
			// Linux
		} else {
			System.setProperty("org.lwjgl.librarypath",
					System.getProperty("user.dir") + "/native/" + System.getProperty("os.name").toLowerCase());
		}

		// Set the StateBasedGame in a app container
		AppGameContainer app = new AppGameContainer(new Launch());

		// Set window configurations and start the app
		app.setDisplayMode(800, 600, false);
		app.setTargetFrameRate(60);
		app.setShowFPS(false);
		app.start();
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		// Add our states to the StateBasedGame, first will be visible at start
		addState(new MainMenuState(MAINMENU_STATE));
		addState(new GameplayState(GAMEPLAY_STATE));
		addState(new Level2(LEVEL2));
		addState(new Level3(LEVEL3));
		addState(new AboutGameState(ABOUTGAME_STATE));
		addState(new LostGameState(GAMEOVER_STATE));
		addState(new WonGameState(WONGAME_STATE));

		// Add them also to the StateBasedEntityManager
		StateBasedEntityManager.getInstance().addState(MAINMENU_STATE);
		StateBasedEntityManager.getInstance().addState(GAMEPLAY_STATE);
		StateBasedEntityManager.getInstance().addState(LEVEL2);
		StateBasedEntityManager.getInstance().addState(LEVEL3);
		StateBasedEntityManager.getInstance().addState(ABOUTGAME_STATE);
		StateBasedEntityManager.getInstance().addState(GAMEOVER_STATE);
		StateBasedEntityManager.getInstance().addState(WONGAME_STATE);
	}

}
