package de.tud.gdi1.discgolf.view;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.model.action.ChangeMapAction;
import de.tud.gdi1.discgolf.model.action.HitFloorAction;
import de.tud.gdi1.discgolf.model.action.ThrowAction;
import de.tud.gdi1.discgolf.model.action.TouchObstacleAction;
import de.tud.gdi1.discgolf.model.entities.Highscore;
import de.tud.gdi1.discgolf.model.factory.BackgroundFactory;
import de.tud.gdi1.discgolf.model.factory.BasketFactory;
import de.tud.gdi1.discgolf.model.factory.DiscFactory;
import de.tud.gdi1.discgolf.model.factory.FloorFactory;
import de.tud.gdi1.discgolf.model.factory.PlayerFactory;
import de.tud.gdi1.discgolf.model.factory.TreeFactory;
import eea.engine.action.basicactions.ChangeStateAction;
import eea.engine.action.basicactions.DestroyEntityAction;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.Event;
import eea.engine.event.basicevents.CollisionEvent;
import eea.engine.event.basicevents.KeyPressedEvent;
import eea.engine.event.basicevents.LeavingScreenEvent;
import eea.engine.event.basicevents.MouseClickedEvent;

public class Level3 extends BasicGameState{
	private static int stateID;
	private StateBasedEntityManager entityManager;
	private Entity player;
	private Entity disc;
	private Entity basket;
	private Entity floor;
	private Entity house;
	private Entity highscore;
	/**
	 * Constructor
	 */
	public Level3(int sid) {
		stateID = sid;
		entityManager = StateBasedEntityManager.getInstance();
	}
	/**
	 * One time before start
	 */
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		// Set background in game
		entityManager.addEntity(stateID, new BackgroundFactory("/assets/background.png").createEntity());

		// Press ESC to go back to main menu
		Entity escListener = new Entity("ESC_Listener");
		KeyPressedEvent escPressed = new KeyPressedEvent(Input.KEY_ESCAPE);
		escPressed.addAction(new ChangeStateAction(Launch.MAINMENU_STATE));
		escListener.addComponent(escPressed);
		entityManager.addEntity(stateID, escListener);

		// Initialize player
		player = new PlayerFactory("/assets/player.png").createEntity();
		entityManager.addEntity(stateID, player);

		// Initialize basket
		basket = new BasketFactory("/assets/basket.png").createEntity();
		entityManager.addEntity(stateID, basket);

		floor = new FloorFactory(0, 425, 8000, 5).createEntity();
		entityManager.addEntity(stateID, floor);
		
		house = new TreeFactory("/assets/house.png", 400, 340).createEntity();
		entityManager.addEntity(stateID, house);

		// Initialize disc
		disc = new DiscFactory("/assets/disc.png", player.getPosition()).createEntity();
		
		// Collision with floor
		CollisionEvent collision = new CollisionEvent();
		collision.addAction(new HitFloorAction(floor, disc));
		disc.addComponent(collision);
		
		// Collision with basket
		CollisionEvent scoreBasket = new CollisionEvent();
		scoreBasket.addAction(new ChangeMapAction(basket));
		disc.addComponent(scoreBasket);
		
		CollisionEvent touchObstacle = new CollisionEvent();
		touchObstacle.addAction(new TouchObstacleAction(house));
		//TODO: Add other obstacles
		disc.addComponent(touchObstacle);
		
		entityManager.addEntity(stateID, disc);

		Entity mouseClickedListener = new Entity("MouseClickedListener");
		MouseClickedEvent mouseClicked = new MouseClickedEvent();

		//mouseClicked.addAction(new ThrowAction(disc, floor, collision, highscore));
		mouseClickedListener.addComponent(mouseClicked);

		Event mainEvent = new LeavingScreenEvent();
		mainEvent.addAction(new DestroyEntityAction());
		disc.addComponent(mainEvent);

		entityManager.addEntity(stateID, mouseClickedListener);
	}

	/**
	 * On every frame
	 */
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		// StateBasedEntityManager renders all entities
		entityManager.renderEntities(container, game, g);
		
	}

	/**
	 * Before every frame
	 */
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		// StateBasedEntityManager updates all entities
		entityManager.updateEntities(container, game, delta);
	}
	@Override
	public int getID() {
		return stateID;
	}
}
