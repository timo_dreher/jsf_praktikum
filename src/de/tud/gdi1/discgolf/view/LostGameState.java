/**
 * 
 */
package de.tud.gdi1.discgolf.view;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.model.factory.BackgroundFactory;
import eea.engine.action.Action;
import eea.engine.action.basicactions.ChangeStateInitAction;
import eea.engine.component.render.ImageRenderComponent;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.ANDEvent;
import eea.engine.event.basicevents.MouseClickedEvent;
import eea.engine.event.basicevents.MouseEnteredEvent;

/**
 * @author Timo
 *
 */
public class LostGameState extends BasicGameState {
	// Initialize the state identifier and entity manager
	private int stateID;
	private StateBasedEntityManager entityManager;

	/**
	 * Constructor
	 */
	public LostGameState(int sid) {
		stateID = sid;
		entityManager = StateBasedEntityManager.getInstance();
	}

	@Override
	public void init(GameContainer container, StateBasedGame sb) throws SlickException {
		// Set background in game
		entityManager.addEntity(stateID, new BackgroundFactory("/assets/game_over.png").createEntity());

		// Back to main menu entity
		String backMain = "Back To Main Menu";
		Entity backMainEntity = new Entity(backMain);

		// Set position and image component
		backMainEntity.setPosition(new Vector2f(60, 500));
		backMainEntity.setScale(0.3f);
		backMainEntity.addComponent(new ImageRenderComponent(new Image("assets/entry.png")));

		// Establish the trigger event and corresponding action
		ANDEvent mainEvents = new ANDEvent(new MouseEnteredEvent(), new MouseClickedEvent());
		Action backMainAction = new ChangeStateInitAction(Launch.MAINMENU_STATE);
		mainEvents.addAction(backMainAction);
		backMainEntity.addComponent(mainEvents);

		// Add backMainEntity to the entityManager
		entityManager.addEntity(stateID, backMainEntity);

		// Try again button
		String tryAgain = "Try Again";
		Entity tryAgainEntity = new Entity(tryAgain);

		// Set position and image component
		tryAgainEntity.setPosition(new Vector2f(60, 430));
		tryAgainEntity.setScale(0.3f);
		tryAgainEntity.addComponent(new ImageRenderComponent(new Image("assets/entry.png")));

		// Establish the trigger event and corresponding action
		ANDEvent mainEvent = new ANDEvent(new MouseEnteredEvent(), new MouseClickedEvent());
		Action tryAgainAction = new ChangeStateInitAction(Launch.GAMEPLAY_STATE);
		mainEvent.addAction(tryAgainAction);
		tryAgainEntity.addComponent(mainEvent);

		// Add tryAgainEntity to the entityManager
		entityManager.addEntity(stateID, tryAgainEntity);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics g) throws SlickException {
		// StateBasedEntityManager renders all entities
		entityManager.renderEntities(gc, sb, g);

		// Button description
		g.drawString("Try Again", 30, 420);
		g.drawString("Back to Main Menu", 30, 490);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta) throws SlickException {
		// StateBasedEntityManager updates all entities
		entityManager.updateEntities(gc, sb, delta);

	}

	@Override
	public int getID() {
		return stateID;
	}

}
