/**
 * 
 */
package de.tud.gdi1.discgolf.view;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import de.tud.gdi1.discgolf.model.factory.BackgroundFactory;
import eea.engine.action.Action;
import eea.engine.action.basicactions.ChangeStateInitAction;
import eea.engine.action.basicactions.QuitAction;
import eea.engine.component.render.ImageRenderComponent;
import eea.engine.entity.Entity;
import eea.engine.entity.StateBasedEntityManager;
import eea.engine.event.ANDEvent;
import eea.engine.event.basicevents.MouseClickedEvent;
import eea.engine.event.basicevents.MouseEnteredEvent;

/**
 * @author Timo Dreher
 *
 */
public class MainMenuState extends BasicGameState {
	// Initialize the state identifier and entity manager
	private int stateID;
	private StateBasedEntityManager entityManager;

	/**
	 * Constructor
	 */
	public MainMenuState(int sid) {
		stateID = sid;
		entityManager = StateBasedEntityManager.getInstance();
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sb) throws SlickException {
		// Set background in game
		entityManager.addEntity(stateID, new BackgroundFactory("/assets/main_menu_background.png").createEntity());

		// Start new game entity
		String newGame = "Start new game";
		Entity newGameEntity = new Entity(newGame);

		// Set position and image component
		newGameEntity.setPosition(new Vector2f(400, 430));
		newGameEntity.setScale(0.3f);
		newGameEntity.addComponent(new ImageRenderComponent(new Image("assets/entry.png")));

		// Establish the trigger event and corresponding action
		ANDEvent mainEvents = new ANDEvent(new MouseEnteredEvent(), new MouseClickedEvent());
		Action newGameAction = new ChangeStateInitAction(Launch.GAMEPLAY_STATE);
		mainEvents.addAction(newGameAction);
		newGameEntity.addComponent(mainEvents);

		// Add newGameEntity to the entityManager
		entityManager.addEntity(stateID, newGameEntity);

		// Quit game entity
		Entity quitGameEntity = new Entity("Quit");

		// Set position and image component
		quitGameEntity.setPosition(new Vector2f(400, 550));
		quitGameEntity.setScale(0.3f);
		quitGameEntity.addComponent(new ImageRenderComponent(new Image("assets/entry.png")));

		// Establish the trigger event and corresponding action
		ANDEvent mainEventsQ = new ANDEvent(new MouseEnteredEvent(), new MouseClickedEvent());
		Action quitGameAction = new QuitAction();
		mainEventsQ.addAction(quitGameAction);
		quitGameEntity.addComponent(mainEventsQ);

		// Add quitGameEntity to the entityManager
		entityManager.addEntity(stateID, quitGameEntity);

		// About game entity
		Entity aboutGameEntity = new Entity("About");

		// Set position and image component
		aboutGameEntity.setPosition(new Vector2f(400, 490));
		aboutGameEntity.setScale(0.3f);
		aboutGameEntity.addComponent(new ImageRenderComponent(new Image("assets/entry.png")));

		// Establish the trigger event and corresponding action
		ANDEvent mainEventsAbout = new ANDEvent(new MouseEnteredEvent(), new MouseClickedEvent());
		Action aboutGameAction = new ChangeStateInitAction(Launch.ABOUTGAME_STATE);
		mainEventsAbout.addAction(aboutGameAction);
		aboutGameEntity.addComponent(mainEventsAbout);

		// Add aboutGameEntity to the entityManager
		entityManager.addEntity(stateID, aboutGameEntity);
	}

	public void render(GameContainer gc, StateBasedGame sb, Graphics g) throws SlickException {
		// StateBasedEntityManager renders all entities
		entityManager.renderEntities(gc, sb, g);

		// Button description
		g.setColor(Color.white);
		g.drawString("New Game", 380, 420);
		g.drawString("About/Controls", 380, 479);
		g.drawString("Quit Game", 380, 538);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta) throws SlickException {
		// StateBasedEntityManager updates all entities
		entityManager.updateEntities(gc, sb, delta);
	}

	@Override
	public int getID() {
		return stateID;
	}

}
